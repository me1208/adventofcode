inputFile=open("day3Input.txt")
inputLines=inputFile.readlines()

runningTot=0
i=0

while i+2<len(inputLines):
    firstLine=set(inputLines[i])
    secondLine=set(inputLines[i+1])
    thirdLine=set(inputLines[i+2])
    i+=3
    commonChar1=firstLine.intersection(secondLine)
    commonChar=(" ".join(commonChar1.intersection(thirdLine))).strip()
    if commonChar.isupper():
        runningTot+=ord(commonChar)-ord("A")+27
    if commonChar.islower():
        runningTot+=ord(commonChar)-ord("a")+1

print(runningTot)
