inputFile=open("day3Input.txt")
inputLines=inputFile.readlines()

runningTot=0

for line in inputLines:
    firstComp=set(line[:int(len(line.strip())/2)])
    secondComp=set(line[int(len(line.strip())/2):])
    commonChar=" ".join(firstComp.intersection(secondComp))
    if commonChar.isupper():
        runningTot+=ord(commonChar)-ord("A")+27
    if commonChar.islower():
        runningTot+=ord(commonChar)-ord("a")+1

print(runningTot)
