inputFile=open("day6Input.txt")
inputLines=inputFile.readlines()
inputString=inputLines[0]

length=14
i=0

while i+length-1<len(inputString):
    testList=[]
    for j in range(length):
        testList.append(inputString[i+j])

    if len(set(testList))==length:
        print(i+length)
        break
    i+=1
