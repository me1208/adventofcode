inputFile=open("day5Input.txt")
inputLines=inputFile.readlines()

i=0

while inputLines[i].strip()[0]!="1":
    i+=1

positions=[]
j=0

while j<len(inputLines[i]):
    if inputLines[i][j]!=" " and inputLines[i][j]!="\n":
        positions.append(j)

    j+=1

stacks={}
for k in range(len(positions)):
    stacks[k+1]=[]

for line in reversed(inputLines[:i]):
    for l in range(len(positions)):
        if line[positions[l]]!=" ":
            stacks[l+1].append(line[positions[l]])

for line in inputLines[i+2:]:
    move=int(line.split()[1])
    start=int(line.split()[3])
    destination=int(line.split()[5])

    m=0
    while m<move and len(stacks[start])!=0:
        stacks[destination].append(stacks[start][-1])
        stacks[start].pop(-1)
        m+=1

for stack in stacks.values():
    print(stack[-1])
