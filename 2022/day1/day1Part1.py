inputFile=open("day1Input.txt")
inputLines=inputFile.readlines()

sumList=[]
runningTot=0

for line in inputLines:
    if line.strip()=="":
        sumList.append(runningTot)
        runningTot=0
    else:
        runningTot+=int(line.strip())

print(max(sumList))
