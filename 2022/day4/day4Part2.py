inputFile=open("day4Input.txt")
inputLines=inputFile.readlines()

count=0

for line in inputLines:
    sectionList=line.strip().split(",")
    section1=sectionList[0].split("-")
    section2=sectionList[1].split("-")
    section1Start=int(section1[0])
    section1End=int(section1[1])
    section2Start=int(section2[0])
    section2End=int(section2[1])

    if (section1Start>=section2Start and section1Start<=section2End) or (section1End>=section2Start and section1End<=section2End) or (section2Start>=section1Start and section2Start<=section1End) or (section2End>=section1Start and section2End<=section1End):
        count+=1

print(count)
